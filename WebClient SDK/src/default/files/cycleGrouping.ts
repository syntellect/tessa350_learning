import { FileGrouping, FileViewModel, GroupInfo } from 'tessa/ui/cards/controls';
import { tryGetFromInfo } from 'tessa/ui';
import { LocalizationManager } from 'tessa/localization';

// tslint:disable: triple-equals

export class CycleGrouping extends FileGrouping {
  //#region ctor

  constructor(name: string, caption: string, isCollapsed: boolean = false) {
    super(name, caption, isCollapsed);
  }

  //#endregion

  //#region methods

  public getGroupInfo(file: FileViewModel): GroupInfo {
    const cycleNumber = tryGetFromInfo<number>(file.model.info, 'KrCycleID');
    const cycleOrder = tryGetFromInfo<number>(file.model.info, 'KrCycleorder');
    const maxCyclewNumber = tryGetFromInfo<number>(file.model.info, 'KrMaxCycleNumber');
    if (cycleNumber != undefined && cycleOrder != undefined && maxCyclewNumber != undefined) {
      return {
        groupId: `CycleGroup${cycleNumber}`,
        groupCaption: LocalizationManager.instance.format(
          '$UI_Controls_FilesControl_CycleGroup',
          cycleNumber
        ),
        order: cycleOrder + 1
      };
    }

    return {
      groupId: 'DocumentsOnApprovalGroup',
      groupCaption: LocalizationManager.instance.localize(
        '$UI_Controls_FilesControl_DocumentsOnApprovalGroup'
      ),
      order: 0
    };
  }

  public clone(): CycleGrouping {
    return new CycleGrouping(this.name, this.caption, this.isCollapsed);
  }

  //#endregion
}
