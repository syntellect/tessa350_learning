﻿#tessa_exchange_format(Version:1, CreatorName:System, CreationTime:2020-01-01T00\:00\:00) {
	#exchange_view(RowID:8144d12b-ac9b-4da7-a21c-4ad1ca355dbe, Alias:DurableRoles, Caption:Все роли кроме контекстных и временных, ModifiedById:11111111-1111-1111-1111-111111111111, ModifiedByName:System, FormatVersion:2, ModifiedDateTime:01.01.2020 0\:00\:00, GroupName:System) {
		#metadata {
			#view(DefaultSortColumn: RoleName, DefaultSortDirection: asc, Paging: always, RowCountSubset: Count, MultiSelect: true, QuickSearchParam: Name)
			#column(Alias: RoleID, Hidden: true, Type: $Roles.ID)
			#column(Alias: RoleName, Caption: $Views_Roles_Role, Type: $Roles.Name, Localizable: true, SortBy: t.Name)
			#column(Alias: TypeName, Caption: $Views_Roles_Type, Type: $RoleTypes.Name, Localizable: true)
			#column(Alias: Info, Caption: $Views_Roles_Info, Hidden: true, Type: String)
			#param(Alias: Name, Caption: $Views_Roles_Name_Param, Multiple: true, Type: $Roles.Name)
			#param(Alias: TypeID, Caption: $Views_Roles_Type_Param, Type: $Roles.TypeID, RefSection: RoleTypes, AllowedOperands: Equality NonEquality) {
				#autocomplete(View: RoleTypes, Param: RoleTypeNameParam, RefPrefix: RoleType, PopupColumns: 1)
				#dropdown(View: RoleTypes, PopupColumns: 1)
			}
			#param(Alias: ShowHidden, Caption: $Views_Roles_ShowHidden_Param, Type: Boolean, AllowedOperands: IsTrue IsFalse)
			#reference(ColPrefix: Role, RefSection: DurableRoles, DisplayValueColumn: RoleName, IsCard: true, OpenOnDoubleClick: true)
			#subset(Alias: RoleTypes, Caption: $Views_Roles_ByType, RefParam: TypeID, RefColumn: ID, CaptionColumn: Name, CountColumn: cnt)
			#subset(Alias: Count)
		}
		#description {}
		#ms_query {
			SELECT
				\#if\(Normal\) \{
				[t].[RoleID]\,
				[t].[RoleName]\,
				[rt].[Name] AS [TypeName]\,
				CASE [t].[TypeID]
					WHEN 1 THEN STUFF\(\(
							SELECT N'\, ' + [r].[Name]
							FROM [RoleUsers] AS [ru] WITH \(NOLOCK\)
							INNER JOIN [Roles] AS [r] WITH \(NOLOCK\)
								ON [r].[ID] = [ru].[ID]
							WHERE [ru].[UserID] = [t].[RoleID]
								AND [r].[TypeID] = 2
							ORDER BY [r].[Name]
							FOR XML PATH\, TYPE
						\).value\(N'.[1]'\, N'nvarchar\(max\)'\)\, 1\, 2\, N''\)
					WHEN 2 THEN COALESCE\(\(
						SELECT [pr].[Name]
						FROM [Roles] AS [r] WITH \(NOLOCK\)
						INNER JOIN [Roles] AS [pr] WITH \(NOLOCK\)
							ON [pr].[ID] = [r].[ParentID]
						WHERE [r].[ID] = [t].[RoleID]\)\,
						[rt].[Name]\)
					ELSE [rt].[Name]
				END AS [Info]
				\}
				\#if\(RoleTypes\) \{
				[t].[ID]\,
				[t].[Name]\,
				count\(*\) AS [cnt]
				\}
				\#if\(Count\) \{
				[t].*
				\}
			FROM \(
				SELECT
					\#if\(Normal\) \{
					[t].[ID] AS [RoleID]\,
					[t].[Name] AS [RoleName]\,
					[t].[TypeID] AS [TypeID]\,
					row_number\(\) OVER \(ORDER BY \#order_by\) AS [rn]
					\}
					\#if\(RoleTypes\) \{
					[rt].[ID]\,
					[lName].[Value] AS [Name]
					\}
					\#if\(Count\) \{
					count\(*\) AS [cnt]
					\}
				FROM [Roles] AS [t] WITH \(NOLOCK\)
				\#if\(RoleTypes\) \{
				INNER JOIN [RoleTypes] AS [rt] WITH \(NOLOCK\)
					ON [rt].[ID] = [t].[TypeID]
				CROSS APPLY [Localization]\([rt].[Name]\, \#param\(locale\)\) AS [lName]
				\}
				WHERE [t].[TypeID] NOT IN \(4\, 6\) /* Не показываем временные роли заданий и контекстные роли */
					\#param\(TypeID\, [t].[TypeID]\)
					\#param\(Name\, [t].[Name]\)
					-- если параметр "показать скрытые" не задан или задан как не "да"\, то не будем отображать скрытые
					\#if\(!ShowHidden || ShowHidden.CriteriaName != "IsTrue"\) \{
					AND [t].[Hidden] = 0
					\}
				\) AS [t]
			\#if\(Normal\) \{
			INNER JOIN [RoleTypes] AS [rt] WITH \(NOLOCK\)
				ON [rt].[ID] = [t].[TypeID]
			\}
			\#if\(PageOffset\) \{
			WHERE [t].[rn] >= \#param\(PageOffset\) AND [t].[rn] < \(\#param\(PageOffset\) + \#param\(PageLimit\)\)
			\}
			\#if\(RoleTypes\) \{
			GROUP BY [t].[ID]\, [t].[Name]
			\}
			\#if\(Normal\) \{
			ORDER BY [t].[rn]
			\}
			\#if\(RoleTypes\) \{
			ORDER BY [t].[Name]
			\}
		}
		#pg_query {
			SELECT
				\#if\(Normal\) \{
				"t"."ID" AS "RoleID"\,
				"t"."Name" AS "RoleName"\,
				"rt"."Name" AS "TypeName"\,
				CASE "t"."TypeID"
					WHEN 1 THEN \(
						SELECT string_agg\("Name"\, '\, '\)
						FROM \(
							SELECT "r"."Name"
							FROM "RoleUsers" AS "ru"
							INNER JOIN "Roles" AS "r"
								ON "r"."ID" = "ru"."ID"
							WHERE "ru"."UserID" = "t"."ID"
								AND "r"."TypeID" = 2
							ORDER BY "r"."Name"\) AS "names"\)
					WHEN 2 THEN COALESCE\(\(
						SELECT "pr"."Name"
						FROM "Roles" AS "r"
						INNER JOIN "Roles" AS "pr"
							ON "pr"."ID" = "r"."ParentID"
						WHERE "r"."ID" = "t"."ID"\)\,
						"rt"."Name"\)
					ELSE "rt"."Name"
				END AS "Info"
				\}
				\#if\(RoleTypes\) \{
				"t"."ID"\,
				"t"."Name"\,
				count\(*\) AS "cnt"
				\}
				\#if\(Count\) \{
				"t".*
				\}
			FROM \(
				SELECT
					\#if\(Normal\) \{
					"t"."ID"\,
					"t"."Name"\,
					"t"."TypeID"
					\}
					\#if\(RoleTypes\) \{
					"rt"."ID"\,
					"lName"."Value" AS "Name"
					\}
					\#if\(Count\) \{
					count\(*\) AS "cnt"
					\}
				FROM "Roles" AS "t"
				\#if\(RoleTypes\) \{
				INNER JOIN "RoleTypes" AS "rt"
					ON "rt"."ID" = "t"."TypeID"
				CROSS JOIN "Localization"\("rt"."Name"\, \#param\(locale\)\) AS "lName"
				\}
				WHERE "t"."TypeID" NOT IN \(4\, 6\) /* Не показываем временные роли заданий и контекстные роли */
					\#param\(TypeID\, "t"."TypeID"\)
					\#param\(Name\, "t"."Name"\)
					-- если параметр "показать скрытые" не задан или задан как не "да"\, то не будем отображать скрытые
					\#if\(!ShowHidden || ShowHidden.CriteriaName != "IsTrue"\) \{
					AND "t"."Hidden" = false
					\}
				\#if\(Normal\) \{
				ORDER BY \#order_by
				\}
				\#if\(PageOffset\) \{
				OFFSET \#param\(PageOffset\) - 1 LIMIT \#param\(PageLimit\)
				\}
				\) AS "t"
			\#if\(Normal\) \{
			INNER JOIN "RoleTypes" AS "rt"
				ON "rt"."ID" = "t"."TypeID"
			ORDER BY \#order_by
			\}
			\#if\(RoleTypes\) \{
			GROUP BY "t"."ID"\, "t"."Name"
			\}
			\#if\(RoleTypes\) \{
			ORDER BY "t"."Name"
			\}
		}
		#role(RoleID:7ff52dc0-ff6a-4c9d-ba25-b562c370004d, ViewID:8144d12b-ac9b-4da7-a21c-4ad1ca355dbe) 
	}
}