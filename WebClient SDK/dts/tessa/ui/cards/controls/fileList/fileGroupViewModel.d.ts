import { FileViewModel } from './fileViewModel';
export declare class FileGroupViewModel {
    constructor(id: string, caption: string, order?: number, isExpanded?: boolean);
    private _isExpanded;
    readonly id: string;
    readonly caption: string;
    order: number;
    get isExpanded(): boolean;
    set isExpanded(value: boolean);
    readonly files: FileViewModel[];
    getState(): any;
    setState(state: any): any;
}
