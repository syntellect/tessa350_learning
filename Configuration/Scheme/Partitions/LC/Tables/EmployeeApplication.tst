﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable ID="dd180b7f-d12c-4cb5-9f19-0b4f2f6ba39e" Partition="29f90c69-c1ef-4cbf-b9d5-7fc91cd68c67">
	<SchemePhysicalColumn Partition="0d95e949-d37e-4318-9868-b6c63c4675c2" ID="6845340c-cd22-4650-864d-adaab0738c28" Name="FullName" Type="String(256) Not Null" />
	<SchemePhysicalColumn Partition="0d95e949-d37e-4318-9868-b6c63c4675c2" ID="690aa568-507c-4aa1-8ce9-1cf34131ce07" Name="Position" Type="String(128) Not Null">
		<Description>Должность</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn Partition="0d95e949-d37e-4318-9868-b6c63c4675c2" ID="72d9b50a-2fdb-4203-9616-a9f426125126" Name="ApplicationText" Type="String(Max) Null" />
	<SchemeComplexColumn Partition="0d95e949-d37e-4318-9868-b6c63c4675c2" ID="4776a1da-80ea-4480-889a-14a10b7176da" Name="State" Type="Reference(Typified) Null" ReferencedTable="47107d7a-3a8c-47f0-b800-2a45da222ff4">
		<Description>Статус заявки</Description>
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="4776a1da-80ea-0080-4000-04a10b7176da" Name="StateID" Type="Int16 Null" ReferencedColumn="502209b0-233f-4e1f-be01-35a50f53414c" />
		<SchemeReferencingColumn ID="8b93a82a-1bfc-4f2b-ae4c-91987d05b373" Name="StateName" Type="String(128) Null" ReferencedColumn="4c1a8dd7-72ed-4fc9-b559-b38ae30dccb9" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn Partition="0d95e949-d37e-4318-9868-b6c63c4675c2" ID="05e05f03-4f12-403e-ae31-fc8a8804e733" Name="BirthDate" Type="Date Not Null" />
	<SchemePhysicalColumn Partition="0d95e949-d37e-4318-9868-b6c63c4675c2" ID="da708dfe-c6ce-4728-ab84-b1d43f4db17a" Name="Number" Type="Int32 Not Null" IsIdentity="true" IdentityStart="1" />
</SchemeTable>