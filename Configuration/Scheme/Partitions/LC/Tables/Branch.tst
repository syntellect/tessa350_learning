﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="0d95e949-d37e-4318-9868-b6c63c4675c2" ID="20041421-681e-411e-8c25-5e6fd4152c3d" Name="Branch" Group="LC" InstanceType="Cards" ContentType="Entries">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="20041421-681e-001e-2000-0e6fd4152c3d" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="20041421-681e-011e-4000-0e6fd4152c3d" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="59e7c7ad-dd0f-4f50-b868-dbccf8f208e8" Name="Name" Type="String(256) Not Null" />
	<SchemeComplexColumn ID="cf0d9550-0b07-4433-a05f-8cffde839cf3" Name="ParentDepartment" Type="Reference(Typified) Not Null" ReferencedTable="81f6010b-9641-4aa5-8897-b8e8603fbf4b">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="cf0d9550-0b07-0033-4000-0cffde839cf3" Name="ParentDepartmentID" Type="Guid Not Null" ReferencedColumn="81f6010b-9641-01a5-4000-08e8603fbf4b" />
		<SchemeReferencingColumn ID="6eaed1ac-c770-4647-b7f9-46741f7bbed4" Name="ParentDepartmentName" Type="String(128) Not Null" ReferencedColumn="616d6b2e-35d5-424d-846b-618eb25962d0" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="3a7e7729-ea2e-4649-82c3-0b5332f6784e" Name="BranchCode" Type="String(256) Null" />
	<SchemeComplexColumn ID="94b1b104-75ab-4b06-a66e-c4b7824e38fa" Name="HeadUser" Type="Reference(Typified) Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="94b1b104-75ab-0006-4000-04b7824e38fa" Name="HeadUserID" Type="Guid Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="e9f65d7e-3b98-463e-bf7b-ef820a4b1045" Name="HeadUserName" Type="String(128) Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="20041421-681e-001e-5000-0e6fd4152c3d" Name="pk_Branch" IsClustered="true">
		<SchemeIndexedColumn Column="20041421-681e-011e-4000-0e6fd4152c3d" />
	</SchemePrimaryKey>
</SchemeTable>