﻿using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Unity;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope
{
    public sealed class KrLifecycleScopeStoreExtension : CardStoreExtension
    {
        #region fields

        private readonly IKrTypesCache krTypesCache;
        private readonly IKrScope scope;
        private KrScopeLevel level;

        #endregion

        #region constructor

        public KrLifecycleScopeStoreExtension(
            IKrTypesCache krTypesCache,
            [Dependency(CardRepositoryNames.ExtendedWithoutTransaction)] ICardRepository cardRepositoryEwt,
            IKrTokenProvider tokenProvider,
            IKrScope scope)
        {
            this.krTypesCache = krTypesCache;
            this.scope = scope;
        }

        #endregion

        #region base overrides

        public override async Task AfterBeginTransaction(
            ICardStoreExtensionContext context)
        {
            if (!context.ValidationResult.IsSuccessful()
                || !await KrComponentsHelper.HasBaseAsync(context.Request.Card.TypeID, this.krTypesCache, context.CancellationToken))
            {
                return;
            }

            this.level = this.scope.EnterNewLevel(context.ValidationResult, false);
        }

        public override async Task AfterRequest(
            ICardStoreExtensionContext context)
        {
            if (this.level != null)
            {
                await this.level.ExitAsync();
                this.level = null;
            }
        }

        #endregion
    }
}