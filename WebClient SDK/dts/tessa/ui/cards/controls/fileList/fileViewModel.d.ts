import { FileTagViewModel } from './fileTagViewModel';
import { IFile } from 'tessa/files';
export declare class FileViewModel {
    constructor(file: IFile);
    private _isLoading;
    private _tag;
    private _toolTip;
    readonly model: IFile;
    readonly id: guid;
    get caption(): string;
    canDownload: boolean;
    get isLoading(): boolean;
    set isLoading(value: boolean);
    get isModified(): boolean;
    get tag(): FileTagViewModel | null;
    set tag(value: FileTagViewModel | null);
    get toolTip(): string;
    set toolTip(value: string);
    private getDefaultToolTip;
}
