﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="0d95e949-d37e-4318-9868-b6c63c4675c2" ID="165eb865-a48f-4f63-a896-0057a231fc97" Name="BranchReaders" Group="LC" InstanceType="Cards" ContentType="Collections">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="165eb865-a48f-0063-2000-0057a231fc97" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="165eb865-a48f-0163-4000-0057a231fc97" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="165eb865-a48f-0063-3100-0057a231fc97" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="b27671c0-80d9-41e7-be67-4dfe3cbefc59" Name="Role" Type="Reference(Typified) Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="b27671c0-80d9-00e7-4000-0dfe3cbefc59" Name="RoleID" Type="Guid Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="17cb5282-72d1-4f17-9fac-ebe33fed9457" Name="RoleName" Type="String(128) Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="165eb865-a48f-0063-5000-0057a231fc97" Name="pk_BranchReaders">
		<SchemeIndexedColumn Column="165eb865-a48f-0063-3100-0057a231fc97" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="165eb865-a48f-0063-7000-0057a231fc97" Name="idx_BranchReaders_ID" IsClustered="true">
		<SchemeIndexedColumn Column="165eb865-a48f-0163-4000-0057a231fc97" />
	</SchemeIndex>
</SchemeTable>