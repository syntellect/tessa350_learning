﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="0d95e949-d37e-4318-9868-b6c63c4675c2" ID="a601757d-f3b7-4be7-a249-7c354ffd1aeb" Name="BranchRegistrators" Group="LC" InstanceType="Cards" ContentType="Collections">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="a601757d-f3b7-00e7-2000-0c354ffd1aeb" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="a601757d-f3b7-01e7-4000-0c354ffd1aeb" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="a601757d-f3b7-00e7-3100-0c354ffd1aeb" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="eadffcc9-7c47-4de0-b14a-a79aa4bb9639" Name="Role" Type="Reference(Typified) Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="eadffcc9-7c47-00e0-4000-079aa4bb9639" Name="RoleID" Type="Guid Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="b7e3992f-e9b8-49a1-92a8-48425d0ba341" Name="RoleName" Type="String(128) Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="a601757d-f3b7-00e7-5000-0c354ffd1aeb" Name="pk_BranchRegistrators">
		<SchemeIndexedColumn Column="a601757d-f3b7-00e7-3100-0c354ffd1aeb" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="a601757d-f3b7-00e7-7000-0c354ffd1aeb" Name="idx_BranchRegistrators_ID" IsClustered="true">
		<SchemeIndexedColumn Column="a601757d-f3b7-01e7-4000-0c354ffd1aeb" />
	</SchemeIndex>
</SchemeTable>