﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Tessa.BusinessCalendar;
using Tessa.Cards;
using Tessa.Extensions.Default.Server.Workflow.KrObjectModel;
using Tessa.Extensions.Default.Server.Workflow.KrProcess;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Extensions.Platform.Server.Cards;
using Tessa.Localization;
using Tessa.Platform;
using Tessa.Platform.Collections;
using Tessa.Platform.Data;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.Roles;
using Unity;

namespace Tessa.Extensions.Default.Server.Workflow.KrCompilers.UserAPI
{
    public static class UserAPIHelper
    {

        public static ValueTask<Guid?> GetCurrentTaskHistoryGroupAsync(
            IKrScript krScipt)
        {
            if (krScipt.Stage != null
                && HandlerHelper.TryGetOverridenTaskHistoryGroup(krScipt.Stage, out var overridenTaskHistoryGroupID))
            {
                return new ValueTask<Guid?>(overridenTaskHistoryGroupID);
            }

            if (krScipt.KrScope.Exists)
            {
                return krScipt.KrScope.GetCurrentHistoryGroupAsync(krScipt.CardID, cancellationToken: krScipt.CancellationToken);
            }

            throw new InvalidOperationException();
        }

        public static async ValueTask<ListStorage<CardRow>> CardRowsAsync(
            IKrScript script,
            string sectionName) => (await script.GetCardObjectAsync()).Sections[sectionName].Rows;

        public static bool IsMainProcess(IKrScript script) => script.ProcessTypeName == KrConstants.KrProcessName;

        public static async ValueTask<bool> IsMainProcessStartedAsync(IKrScript script)
        {
            if (IsMainProcess(script))
            {
                return true;
            }

            var contextualSatellite = await script.GetContextualSatelliteAsync();

            if (contextualSatellite is null)
            {
                return false;
            }

            return await script.Db.SetCommand(
                    script.DbScope.BuilderFactory
                        .Select().Top(1)
                        .V(1)
                        .From("WorkflowProcesses").NoLock()
                        .Where().C("ID").Equals().P("ID")
                        .And().C("TypeName").Equals().V(KrConstants.KrProcessName)
                        .Limit(1)
                        .Build(),
                    script.Db.Parameter("ID", contextualSatellite.ID))
                .LogCommand()
                .ExecuteAsync<bool>(script.CancellationToken);
        }

        public static async ValueTask<bool> IsMainProcessInactiveAsync(
            IKrScript krScript,
            Card contextualSatellite)
        {
            if (krScript.IsMainProcess())
            {
                return false;
            }

            if (contextualSatellite != null)
            {
                return contextualSatellite
                    .GetStagesSection()
                    .Rows
                    .All(p => (p.TryGet<int?>(KrConstants.StateID) ?? KrStageState.Inactive.ID) == KrStageState.Inactive);
            }

            var hasAtLeastNonInactiveStage = await krScript
                .Db
                .SetCommand(krScript.DbScope.BuilderFactory
                    .Select().Top(1)
                    .V(true)
                    .From(KrConstants.KrStages.Name, "s").NoLock()
                    .InnerJoin(KrConstants.KrApprovalCommonInfo.Name, "aci").NoLock().On().C("aci", KrConstants.ID).Equals().C("s", KrConstants.ID)
                    .Where().C("aci", KrConstants.KrProcessCommonInfo.MainCardID).Equals().P("ID")
                        .And().C("s", KrConstants.KrStages.StageStateID).NotEquals().V(KrStageState.Inactive.ID)
                    .Limit(1)
                    .Build(),
                    krScript.Db.Parameter("ID", krScript.CardID))
                .LogCommand()
                .ExecuteAsync<bool>(krScript.CancellationToken);
            return !hasAtLeastNonInactiveStage;
        }

        public static T Resolve<T>(IUnityContainer unityContainer, string name = null)
        {
            return unityContainer.Resolve<T>(name);
        }

        public static void ForEachStage(
           IKrScript script,
           Action<CardRow> rowAction,
           bool withNesteds)
        {
            IEnumerable<CardRow> rows = script.ProcessHolderSatellite.GetStagesSection().Rows;
            if (!withNesteds)
            {
                rows = rows.Where(p => p.Fields.TryGet<Guid?>(KrConstants.KrStages.NestedProcessID) == null);
            }

            foreach (var row in rows)
            {
                rowAction(row);
            }
        }

        public static async Task ForEachStageInMainProcessAsync(
            IKrScript script,
            Func<CardRow, Task> rowActionAsync,
            bool withNesteds)
        {
            IEnumerable<CardRow> rows = (await script.GetContextualSatelliteAsync()).GetStagesSection().Rows;
            if (!withNesteds)
            {
                rows = rows.Where(p => p.Fields.TryGet<Guid?>(KrConstants.KrStages.NestedProcessID) == null);
            }

            foreach (var row in rows)
            {
                await rowActionAsync(row);
            }
        }

        public static async Task SetStageStateAsync(
            IKrScript script,
            CardRow stage,
            KrStageState stageState)
        {
            stage.Fields["StateID"] = (int)stageState;
            stage.Fields["StateName"] = await script.CardMetadata.GetStageStateNameAsync(stageState, script.CancellationToken);
        }

        public static Stage GetOrAddStage(IKrScript script, string name, StageTypeDescriptor descriptor, int pos = int.MaxValue, bool ignoreManualChanges = false) =>
            AddStageInternal(script, name, descriptor, pos, ignoreManualChanges, true);

        public static Stage AddStage(IKrScript script, string name, StageTypeDescriptor descriptor, int pos = int.MaxValue, bool ignoreManualChanges = false) =>
            AddStageInternal(script, name, descriptor, pos, ignoreManualChanges, false);

        public static bool RemoveStage(IKrScript script, string name, bool ignoreManualChanges = false)
        {
            if (script.StagesContainer is null)
            {
                throw new InvalidOperationException(
                    $"{nameof(RemoveStage)} can be invoked only in route calculating context.");
            }

            var cnt = script.StagesContainer.Stages
                .RemoveAll(stage =>
                    stage.TemplateID == script.TemplateID
                    && !stage.BasedOnTemplateStage
                    && stage.BasedOnTemplate
                    && stage.Name == name
                    && (!stage.RowChanged || ignoreManualChanges));
            return cnt != 0;
        }

        public static void SetSinglePerformer(
            Guid id,
            string name,
            Stage intoStage,
            bool ignoreManualChanges = false)
        {
            id = id != default
                ? id
                : throw new NullReferenceException(LocalizationManager.Format("$KrMessages_StageTemplateNullReferenceException", "Performer ID"));
            name = name
                ?? throw new NullReferenceException(LocalizationManager.Format("KrMessages_StageTemplateNullReferenceException", "Performer Name"));
            intoStage = intoStage
                ?? throw new NullReferenceException(LocalizationManager.Format("KrMessages_StageTemplateNullReferenceException", "Into Stage"));

            if (ignoreManualChanges || !intoStage.RowChanged)
            {
                intoStage.Performer = new Performer(id, name);
            }
        }

        public static void ResetSinglePerformer(
            Stage stage,
            bool ignoreManualChanges = false)
        {
            stage = stage
                ?? throw new NullReferenceException(LocalizationManager.Format("KrMessages_StageTemplateNullReferenceException", "stage"));

            if (ignoreManualChanges || !stage.RowChanged)
            {
                stage.Performer = null;
            }
        }

        public static Performer AddPerformer(
            IKrScript script,
            Guid id,
            string name,
            Stage intoStage,
            int pos = int.MaxValue,
            bool ignoreManualChanges = false)
        {
            id = id != default
                ? id
                : throw new NullReferenceException(LocalizationManager.Format("$KrMessages_StageTemplateNullReferenceException", "Performer ID"));
            name = name
                ?? throw new NullReferenceException(LocalizationManager.Format("KrMessages_StageTemplateNullReferenceException", "Performer Name"));
            intoStage = intoStage
                ?? throw new NullReferenceException(LocalizationManager.Format("KrMessages_StageTemplateNullReferenceException", "Into Stage"));

            pos = NormalizePos(pos, intoStage.Performers);

            // Если этап не изменен, то у него есть предок, в котором можно найти исполнителя
            // Если этап изменен, то он не заменяется. Можно просто взять сам этап и посмотреть там.
            var ancestor = !intoStage.RowChanged
                ? intoStage.Ancestor
                : intoStage;
            if (!ignoreManualChanges
                && ancestor?.RowChanged == true)
            {
                return null;
            }
            // В старом этапе может быть такая же роль.
            var oldPerformer = ancestor?.Performers
                ?.FirstOrDefault(p =>
                    p.PerformerID == id
                    && p.PerformerName == name
                    && !p.IsSql);

            var newPerformer = new MultiPerformer(oldPerformer?.RowID ?? Guid.NewGuid(), id, name, intoStage.RowID);
            var offset = Convert.ToInt32(oldPerformer != null && intoStage.Performers.Remove(oldPerformer));
            intoStage.Performers.Insert(pos - offset, newPerformer);
            return newPerformer;
        }

        public static void RemovePerformer(
            Guid[] ids,
            Stage stage,
            bool ignoreManualChanges = false)
        {
            if (ignoreManualChanges || !stage.RowChanged)
            {
                stage.Performers.RemoveAll(p => ids.Contains(p.RowID));
            }
        }

        public static async Task AddTaskHistoryRecordAsync(
            IKrScript script,
            Guid? taskHistoryGroupID,
            Guid typeID,
            string typeName,
            string typeCaption,
            Guid optionID,
            string result = default,
            Guid? performerID = default,
            string performerName = default,
            int? cycle = default,
            int? timeZoneID = default,
            TimeSpan? timeZoneUtcOffset = default,
            Func<CardTaskHistoryItem, ValueTask> modifyActionAsync = default)
        {
            var cardMetadata = script.CardMetadata;
            var card = await script.GetCardObjectAsync();
            var contextualSatellite = await script.GetContextualSatelliteAsync();
            var cycleInternal = cycle ?? await script.GetCycleAsync();
            var perfIDInternal = performerID ?? script.Session.User.ID;
            var perfNameInternal = performerName ?? script.Session.User.Name;
            var option = (await cardMetadata.GetEnumerationsAsync(script.CancellationToken)).CompletionOptions[optionID];
            var utcNow = DateTime.UtcNow;

            if (!timeZoneID.HasValue || !timeZoneUtcOffset.HasValue)
            {
                var timeZonesCard = await script.CardCache.Cards.GetAsync("TimeZones", script.CancellationToken);
                var defaultTimeZoneSection = timeZonesCard.Sections[TimeZonesHelper.DefaultTimeZoneSection];

                timeZoneID = TimeZonesHelper.DefaultZoneID;
                timeZoneUtcOffset = TimeSpan.FromMinutes(defaultTimeZoneSection.Fields.Get<int>("UtcOffsetMinutes"));
            }

            var item = new CardTaskHistoryItem
            {
                GroupRowID = taskHistoryGroupID,
                State = CardTaskHistoryState.Inserted,
                RowID = Guid.NewGuid(),
                TypeID = typeID,
                TypeName = typeName,
                TypeCaption = typeCaption,
                Created = utcNow,
                Planned = utcNow,
                InProgress = utcNow,
                Completed = utcNow,
                CompletedByID = perfIDInternal,
                CompletedByName = perfNameInternal,
                AuthorID = perfIDInternal,
                AuthorName = perfNameInternal,
                UserID = perfIDInternal,
                UserName = perfNameInternal,
                RoleID = perfIDInternal,
                RoleName = perfNameInternal,
                RoleTypeID = RoleHelper.PersonalRoleTypeID,
                OptionID = option.ID,
                OptionName = option.Name,
                OptionCaption = option.Caption,
                Result = result ?? string.Empty,
                TimeZoneID = timeZoneID,
                TimeZoneUtcOffsetMinutes = (int?)timeZoneUtcOffset.Value.TotalMinutes
            };

            if (modifyActionAsync != null)
            {
                await modifyActionAsync(item);
            }
            card.TaskHistory.Add(item);
            contextualSatellite.AddToHistory(item.RowID, cycleInternal > 0 ? cycleInternal : 1);
        }

        public static CardTaskHistoryGroup ResolveTaskHistoryGroup(
            IKrScript script,
            Guid groupTypeID,
            Guid? parentGroupTypeID = null,
            bool newIteration = false) => script.ResolveTaskHistoryGroup(groupTypeID, parentGroupTypeID, newIteration);

        public static async ValueTask<int> GetCycleAsync(IKrScript script)
        {
            if (script.ProcessTypeName == KrConstants.KrProcessName)
            {
                // Для основного процесса цикл лежит в его инфо.
                return script.WorkflowProcess.InfoStorage.TryGet<int?>(KrConstants.Keys.Cycle) ?? 0;
            }

            var serializer = script.StageSerializer;
            return ProcessInfoCacheHelper.Get(serializer, await script.GetContextualSatelliteAsync())?.TryGet<int?>(KrConstants.Keys.Cycle) ?? 0;
        }

        public static async ValueTask<bool> SetCycleAsync(
            IKrScript script,
            int newValue)
        {
            if (newValue < 1)
            {
                return false;
            }

            if (script.ProcessTypeName == KrConstants.KrProcessName)
            {
                // Для основного процесса цикл лежит в его инфо.
                script.WorkflowProcess.InfoStorage[KrConstants.Keys.Cycle] = Int32Boxes.Box(newValue);
                return true;
            }

            var serializer = script.StageSerializer;
            var mainProcessInfo = ProcessInfoCacheHelper.Get(serializer, await script.GetContextualSatelliteAsync());
            mainProcessInfo[KrConstants.Keys.Cycle] = Int32Boxes.Box(newValue);
            return true;
        }

        public static bool HasKrComponents(
            IKrScript script,
            KrComponents[] components)
        {
            var allComponents = KrComponents.None;
            for (var i = 0; i < components.Length; i++)
            {
                allComponents |= components[i];
            }

            return HasKrComponents(script, allComponents);
        }

        public static bool HasKrComponents(
            IKrScript script,
            KrComponents components)
        {
            return (script.KrComponents & components) == components;
        }

        public static async ValueTask<ISerializableObject> GetPrimaryProcessInfoAsync(
            IKrScript script,
            Guid? cardID)
        {
            var satellite = cardID.HasValue
                ? await script.KrScope.GetKrSatelliteAsync(cardID.Value, script.ValidationResult)
                : await script.GetContextualSatelliteAsync();
            return ProcessInfoCacheHelper.Get(script.StageSerializer, satellite);
        }

        public static async ValueTask<ISerializableObject> GetSecondaryProcessInfoAsync(
            IKrScript script,
            Guid secondaryProcessID,
            Guid? mainCardID)
        {
            Card satellite;
            if (secondaryProcessID == script.ProcessID)
            {
                satellite = script.ProcessHolderSatellite;
            }
            else
            {
                satellite = await script.KrScope.GetSecondaryKrSatelliteAsync(secondaryProcessID, script.CancellationToken);
                var satelliteCardID = satellite
                    .GetApprovalInfoSection()
                    .RawFields
                    .TryGet<Guid?>(KrConstants.KrProcessCommonInfo.MainCardID);
                if (satelliteCardID != (mainCardID ?? script.CardID))
                {
                    throw new InvalidOperationException("Secondary satellite has different main card id");
                }
            }

            return ProcessInfoCacheHelper.Get(script.StageSerializer, satellite);
        }

        public static ValueTask<Card> GetNewCardAsync(IKrScript script)
        {
            return GetNewCardAccessStrategy(script).GetCardAsync(withoutTransaction: true);
        }
        
        public static IMainCardAccessStrategy GetNewCardAccessStrategy(IKrScript script)
        {
            return script.Stage.InfoStorage.Get<IMainCardAccessStrategy>(KrConstants.Keys.NewCard);
        }

        public static IDictionary<string, object> GetProcessInfoForBranch(
            IKrScript script,
            Guid rowID)
        {
            var infos = script.StageInfoStorage.TryGet<IDictionary<string, object>>(KrConstants.Keys.ForkNestedProcessInfo);
            if (infos is null)
            {
                return null;
            }

            var key = rowID.ToString("D");
            var info = infos.TryGet<IDictionary<string, object>>(key);
            if (info != null)
            {
                return info;
            }

            info = new Dictionary<string, object>();
            infos[key] = info;
            return info;
        }

        /// <summary>
        /// Асинхронно подготавливает файлы карточки диалога к сохранению.
        /// </summary>
        /// <param name="dbScope">Объект для взаимодействия с базой данных.</param>
        /// <param name="cardRepository">Репозиторий для управления карточками.</param>
        /// <param name="cardTaskDialogActionResult">Результат завершения этапа "Диалог".</param>
        /// <param name="mainCardAccessStrategy">Стратегия для доступа к основной картчоке.</param>
        /// <param name="dialogCardAccessStrategy">Стратегия для доступа к карточке диалога.</param>
        /// <param name="validationResult">Результат валидации.</param>
        /// <param name="cancellationToken">Объект, постредством которого можно отменить асинхронную задачу.</param>
        public static async Task PrepareFileInDialogCardForStoreAsync(
            IDbScope dbScope,
            ICardRepository cardRepository,
            CardTaskDialogActionResult cardTaskDialogActionResult,
            IMainCardAccessStrategy mainCardAccessStrategy,
            IMainCardAccessStrategy dialogCardAccessStrategy,
            IValidationResultBuilder validationResult,
            CancellationToken cancellationToken = default)
        {
            Check.ArgumentNotNull(dbScope, nameof(dbScope));
            Check.ArgumentNotNull(cardRepository, nameof(cardRepository));
            Check.ArgumentNotNull(cardTaskDialogActionResult, nameof(cardTaskDialogActionResult));
            Check.ArgumentNotNull(mainCardAccessStrategy, nameof(mainCardAccessStrategy));
            Check.ArgumentNotNull(dialogCardAccessStrategy, nameof(dialogCardAccessStrategy));
            Check.ArgumentNotNull(validationResult, nameof(validationResult));

            if (cardTaskDialogActionResult.StoreMode == CardTaskDialogStoreMode.Settings
                && dialogCardAccessStrategy.WasFileContainerUsed)
            {
                var mainCard = await mainCardAccessStrategy.GetCardAsync(validationResult, cancellationToken: cancellationToken);
                var mainCardFileContainer = await mainCardAccessStrategy.GetFileContainerAsync(validationResult, cancellationToken: cancellationToken);
                var dialogCard = await dialogCardAccessStrategy.GetCardAsync(validationResult, cancellationToken: cancellationToken);
                var dialogFileContainer = await dialogCardAccessStrategy.GetFileContainerAsync(validationResult, cancellationToken: cancellationToken);
                var satelliteID = await FileSatelliteHelper.GetFileSatelliteID(
                    cardRepository,
                    dbScope,
                    mainCard.ID,
                    validationResult,
                    true,
                    cancellationToken);

                foreach (var file in dialogFileContainer.FileContainer.Files)
                {
                    mainCardFileContainer.FileContainer.Files.Add(file);
                }

                foreach (var file in dialogCard.Files)
                {
                    file.TaskID = cardTaskDialogActionResult.TaskID;

                    var fileOptions = file.DeserializeOptions();
                    fileOptions[FileSatelliteHelper.KeepFileMark] = BooleanBoxes.Box(cardTaskDialogActionResult.KeepFiles);
                    file.Options = fileOptions.SerializeJson();

                    var mainCardFile = mainCard.Files.Add(file);
                    mainCardFile.Info[FileSatelliteHelper.FileSatelliteFileKey] = BooleanBoxes.True;

                    file.ExternalSource = new CardFileContentSource
                    {
                        CardID = satelliteID.Value,
                        FileID = file.RowID,
                        VersionRowID = file.VersionRowID,
                        Source = file.StoreSource,
                        CardTypeID = CardHelper.FileSatelliteTypeID,
                    };
                    file.State = CardFileState.None;
                }
            }
        }

        private static Stage AddStageInternal(
            IKrScript script,
            string name,
            StageTypeDescriptor descriptor,
            int pos,
            bool ignoreManualChanges,
            bool returnOldStage)
        {
            if (script.StagesContainer is null)
            {
                throw new InvalidOperationException(
                    $"{nameof(AddStage)} can be invoked only in route calculating context.");
            }

            name = name
                ?? throw new NullReferenceException(LocalizationManager.Format("KrMessages_StageTemplateNullReferenceException", "Stage Name"));
            var currentStages = script.CurrentStages;
            pos = NormalizePos(pos, currentStages);

            var oldStage = script.StagesContainer.InitialStages
                .FirstOrDefault(initialStage =>
                    initialStage.TemplateID == script.TemplateID
                    && !initialStage.BasedOnTemplateStage
                    && initialStage.BasedOnTemplate
                    && initialStage.Name == name);

            if (!ignoreManualChanges
                && (oldStage?.RowChanged == true
                    || oldStage?.OrderChanged == true))
            {
                if (returnOldStage)
                {
                    var copiedStage = new Stage(oldStage);
                    // Чтобы oldStage стал предком для copiedStage
                    copiedStage.Inherit(oldStage);
                    copiedStage.TemplateStageOrder = pos;
                    var index = script.Stages.IndexOf(oldStage);
                    if (index != -1)
                    {
                        script.StagesContainer.ReplaceStage(index, copiedStage);
                    }
                    else
                    {
                        script.StagesContainer.InsertStage(copiedStage);
                    }
                    return copiedStage;
                }
                return null;
            }
            for (var index = pos; index < currentStages.Count; index++)
            {
                currentStages[index].TemplateStageOrder++;
            }
            var newStage = new Stage(
                oldStage?.ID ?? Guid.NewGuid(),
                name,
                descriptor.ID,
                descriptor.Caption,
                script.StageGroupID,
                script.StageGroupOrder,
                script.TemplateID,
                script.TemplateName,
                script.Order,
                script.CanChangeOrder,
                script.Position,
                oldStage,
                script.IsStagesReadonly);
            newStage.TemplateStageOrder = pos;
            script.StagesContainer.InsertStage(newStage);
            return newStage;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int NormalizePos(
            int pos,
            ICollection collection)
        {
            if (pos < 0)
            {
                pos = 0;
            }
            else if (collection.Count < pos)
            {
                pos = collection.Count;
            }

            return pos;
        }

    }
}