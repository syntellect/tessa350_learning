﻿#tessa_exchange_format(Version:1, CreatorName:System, CreationTime:2020-01-01T00\:00\:00) {
	#exchange_view(RowID:871ad32d-0846-448c-beb2-85c1d357177b, Alias:ReportCurrentTasksByUser, Caption:Текущие задания по пользователям, ModifiedById:11111111-1111-1111-1111-111111111111, ModifiedByName:System, FormatVersion:2, ModifiedDateTime:01.01.2020 0\:00\:00, GroupName:System) {
		#metadata {
			#view(DefaultSortColumn: UserName, DefaultSortDirection: asc, Paging: always, ExportDataPageLimit: 1000, SelectionMode: cell)
			#column(Alias: UserID, Hidden: true, Type: $PersonalRoles.ID)
			#column(Alias: UserName, Caption: $Views_ReportCurrentTasksByUser_User, Type: $PersonalRoles.Name, SortBy: t.UserName)
			#column(Alias: New, Caption: $Views_ReportCurrentTasksByUser_New, Type: Int64, SortBy: t.New)
			#column(Alias: NewDelayed, Caption: $Views_ReportCurrentTasksByUser_NewDelayed, Type: Int64, SortBy: t.NewDelayed)
			#column(Alias: NewAvgDelayPeriod, Caption: $Views_ReportCurrentTasksByUser_NewAvgDelayPeriod, Type: Int64, SortBy: t.NewAvgDelayPeriod)
			#column(Alias: InWork, Caption: $Views_ReportCurrentTasksByUser_InWork, Type: Int64, SortBy: t.InWork)
			#column(Alias: InWorkDelayed, Caption: $Views_ReportCurrentTasksByUser_InWorkDelayed, Type: Int64, SortBy: t.InWorkDelayed)
			#column(Alias: InWorkAvgDelayPeriod, Caption: $Views_ReportCurrentTasksByUser_InWorkAvgDelayPeriod, Type: Int64, SortBy: t.InWorkAvgDelayPeriod)
			#column(Alias: Postponed, Caption: $Views_ReportCurrentTasksByUser_Postponed, Type: Int64, SortBy: t.Postponed)
			#column(Alias: PostponedDelayed, Caption: $Views_ReportCurrentTasksByUser_PostponedDelayed, Type: Int64, SortBy: t.PostponedDelayed)
			#column(Alias: PostponedAvgDelayPeriod, Caption: $Views_ReportCurrentTasksByUser_PostponedAvgDelayPeriod, Type: Int64, SortBy: t.PostponedAvgDelayPeriod)
			#column(Alias: Total, Caption: $Views_ReportCurrentTasksByUser_Total, Type: Int64, SortBy: t.Total)
			#column(Alias: rn, Hidden: true, Type: Int64)
			#param(Alias: EndDate, Caption: $Views_ReportCurrentTasksByUser_EndDate_Param, Hidden: true, Multiple: true, Type: $Tasks.Planned)
			#param(Alias: CreationDate, Caption: $Views_ReportCurrentTasksByUser_CreationDate_Param, Hidden: true, Multiple: true, Type: $Tasks.Created)
			#param(Alias: TypeParam, Caption: $Views_ReportCurrentTasksByUser_Type_Param, Hidden: true, Type: $DocumentCommonInfo.CardTypeID, RefSection: TypeForView, AllowedOperands: Equality) {
				#autocomplete(View: KrTypesEffective, Param: Caption, PopupColumns: 1)
				#dropdown(View: KrTypesEffective, PopupColumns: 1)
			}
			#param(Alias: TaskType, Caption: $Views_ReportCurrentTasksByUser_TaskType_Param, Hidden: true, Multiple: true, Type: $Tasks.TypeID, RefSection: TaskTypes) {
				#autocomplete(View: TaskTypes, Param: Caption)
				#dropdown(View: TaskTypes, PopupColumns: 1)
			}
			#param(Alias: Department, Caption: $Views_ReportCurrentTasksByUser_Department_Param, Hidden: true, Multiple: true, Type: $RoleUsers.ID, RefSection: DepartmentRoles) {
				#autocomplete(View: Departments, Param: Name, PopupColumns: 1 3 5)
			}
			#param(Alias: SelUser, Caption: $Views_ReportCurrentTasksByUser_User_Param, Hidden: true, Multiple: true, Type: $RoleUsers.UserID, RefSection: PersonalRoles) {
				#autocomplete(View: Users, Param: Name, PopupColumns: 1 4)
			}
			#param(Alias: Role, Caption: $Views_CompletedTasks_RoleGroup_Param, Hidden: true, Multiple: true, Type: $RoleUsers.ID, RefSection: Roles) {
				#autocomplete(View: DurableRoles, Param: Name, RefPrefix: Role, PopupColumns: 1 3)
			}
		}
		#description {}
		#ms_query {
			DECLARE @TotalID					uniqueidentifier;
			DECLARE @SystemUserID				uniqueidentifier;
			DECLARE @NoRowsInParentDepartment	uniqueidentifier;

			\#if\(Normal\) \{
			DECLARE @NowTimestamp				datetime;
			\}

			SET @TotalID						= '00000000-0000-0000-0000-000000000000';
			SET @SystemUserID					= '11111111-1111-1111-1111-111111111111';
			SET @NoRowsInParentDepartment		= '11111111-1111-1111-1111-111111111111'; -- используется для предотвращение показа строк\, если их нет в родительском представлении

			\#if\(Normal\) \{
			SET @NowTimestamp = GETUTCDATE\(\);
			\}

			WITH [TasksAndDepts] \([UserID]\, [UserName]\, [StateID]\, [DelayIndex]\, [sum]\, [cnt]\) AS \(
				SELECT
					[pr].[ID]\,
					[pr].[Name]\,
					[t].[StateID]\,
					[t].[DelayIndex]\,
					[t].[sum]\,
					[t].[cnt]
				FROM \(
					SELECT
						[t].[StateID]\,
						[t].[RoleID]\,
						[t].[UserID]\,
						[t].[DelayIndex]\,
						sum\([t].[Quant]\)		AS [sum]\,
						count\(*\)				AS [cnt]
					FROM \(
						SELECT
							[t].[StateID]\,
							[t].[RoleID]\,
							[t].[UserID]\,
							CASE WHEN [qq1].[QuantNumber] - [qq].[QuantNumber] > 0
								THEN 1
								ELSE 0
							END AS [DelayIndex]\,
							\([qq1].[QuantNumber] - [qq].[QuantNumber]\) / 4 AS [Quant]
						FROM [Tasks] AS [t] WITH \(NOLOCK\)
						\#if\(TypeParam\) \{
						LEFT JOIN [DocumentCommonInfo] AS [i] WITH \(NOLOCK\)
							ON [i].[ID] = [t].[ID]
						\}
						OUTER APPLY \(
							SELECT TOP \(1\) [q].[QuantNumber]
							FROM [CalendarQuants] AS [q] WITH \(NOLOCK\)
							WHERE [q].[StartTime] <= DATEADD\(minute\, [t].[TimeZoneUtcOffsetMinutes]\, [t].[Planned]\)
							ORDER BY [q].[StartTime] DESC
							\) AS [qq]
						OUTER APPLY \(
							SELECT TOP \(1\) [q].[QuantNumber]
							FROM [CalendarQuants] AS [q] WITH \(NOLOCK\)
							WHERE [q].[StartTime] <= DATEADD\(minute\, [t].[TimeZoneUtcOffsetMinutes]\, @NowTimestamp\)
							ORDER BY [q].[StartTime] DESC
							\) AS [qq1]
						WHERE 1 = 1
							\#param\(EndDate\, [t].[Planned]\)
							\#param\(CreationDate\, [t].[Created]\)
							\#param\(TaskType\, [t].[TypeID]\)
							\#if\(TypeParam\) \{
							AND COALESCE\([i].[DocTypeID]\, [i].[CardTypeID]\) = \#param\(TypeParam\)
							\}
						\) AS [t]
					GROUP BY [StateID]\, [RoleID]\, [UserID]\, [DelayIndex]
					\) AS [t]
				INNER JOIN [RoleUsers] AS [ru] WITH \(NOLOCK\)
					-- Или сотрудник входит в роль и задание на в работе\, или он является исполнителем задания
					ON [ru].[ID] = COALESCE\([t].[UserID]\, [t].[RoleID]\)
					AND [ru].[UserID] <> @SystemUserID
				INNER JOIN [PersonalRoles] AS [pr] WITH \(NOLOCK\)
					ON [pr].[ID] = [ru].[UserID] -- решает задвоение результатов при переименовании пользователей
				/*
				Джойн для отработки замещения конкретного пользователя в ролях для заданий в работе \(в т.ч. персональной роли\).
				Пользователь будет видеть задания в работе тех сотрудников\, кого он замещает.
				Система заботится о том чтобы периоды замещения не пересекались\, поэтому строчки не множатся.
				Используется OUTER APPLY вместо LEFT JOIN\, т.к. в RoleDeputies могут быть несколько строк по разным замещаемым сотрудникам.
				*/
				OUTER APPLY \(
					SELECT TOP \(1\) [rd].[DeputyID]
					FROM [RoleDeputies] AS [rd] WITH \(NOLOCK\)
					WHERE [rd].[ID] = [t].[RoleID]										-- Заместитель видит только задания в работе на определенную роль
						AND [rd].[IsActive] = 1											-- Только активные замещения - выставляется сервисом
						AND [rd].[DeputyID] = [ru].[UserID]								-- для работы отчета по заданиям других пользователей
						AND \(
							[rd].[TypeID] = 0 AND [t].[UserID] IS NULL OR				-- Замещение в стат. роли\, не в работе
							[rd].[TypeID] = 0 AND [t].[UserID] = [rd].[DeputizedID] OR	-- Замещение в стат. роли\, в работе у замещаемого
							[rd].[TypeID] = 1\)											-- Замещение персональной роли
					\) AS [rd]
				LEFT JOIN [RoleUsers] AS [rud] WITH \(NOLOCK\)
					ON [rud].[TypeID] = 2
					AND [rud].[UserID] = [ru].[UserID]
				LEFT JOIN [Roles] AS [r] WITH \(NOLOCK\)
					ON [r].[ID] = [rud].[ID]
				INNER JOIN \(
					-- Для показа только заданий в своих подразделениях\, если юзер - глава\, или только своих.
					-- Работает только при наличии метароли - все сотрудники на все уровни вглубь
					SELECT [ru].[UserID]
					FROM [DepartmentRoles] AS [dr] WITH \(NOLOCK\)
					INNER JOIN [MetaRoles] AS [mr] WITH \(NOLOCK\)
						ON [mr].[IDGuid] = [dr].[ID]
					INNER JOIN [RoleUsers] AS [ru] WITH \(NOLOCK\)
						ON [ru].[ID] = [mr].[ID]
					WHERE [dr].[HeadUserID] = \#param\(CurrentUserID\)

					UNION

					-- Добавляем сотрудников подразделений\, для которых нет метаролей и текущий = руководитель
					SELECT [ru].[UserID]
					FROM [DepartmentRoles] AS [dr] WITH \(NOLOCK\)
					INNER JOIN [RoleUsers] AS [ru] WITH \(NOLOCK\)
						ON [ru].[ID] = [dr].[ID]
					WHERE [dr].[HeadUserID] = \#param\(CurrentUserID\)
						AND NOT EXISTS \(
							SELECT NULL
							FROM [MetaRoles] AS [mr] WITH \(NOLOCK\)
							WHERE [mr].[IDGuid] = [dr].[ID]\)

					UNION

					-- Может смотреть отчёт по своим заданиям
					SELECT \#param\(CurrentUserID\)

					UNION

					-- Учёт карточек настройки прав на отчёт
					\#if\(Administrator\) \{
					-- Админ видит всех сотрудников\, входящих в роль "Все сотрудники"\, т.е. обычно всех\, кроме System
					SELECT [rua].[UserID]
					FROM [RoleUsers] AS [rua] WITH \(NOLOCK\)
					WHERE [rua].[ID] = '7ff52dc0-ff6a-4c9d-ba25-b562c370004d'
					\} \{
					SELECT DISTINCT [rup].[UserID]
					FROM [ReportRolesActive] AS [rra] WITH \(NOLOCK\)
					INNER JOIN [RoleUsers] AS [rua] WITH \(NOLOCK\)
						ON [rua].[ID] = [rra].[RoleID]
					INNER JOIN [ReportRolesPassive] AS [rrp] WITH \(NOLOCK\)
						ON [rrp].[ID] = [rra].[ID]
					INNER JOIN [RoleUsers] AS [rup] WITH \(NOLOCK\)
						ON [rup].[ID] = [rrp].[RoleID]
					WHERE [rua].[UserID] = \#param\(CurrentUserID\)
					\}
					\) AS [wr] ON [wr].[UserID] = [ru].[UserID]
				\#if\(Role\) \{
				INNER JOIN [RoleUsers] AS [fr] WITH \(NOLOCK\)
					ON [fr].[UserID] = [ru].[UserID]
					\#param\(Role\, [fr].[ID]\)
				\}
				WHERE \(
					[t].[UserID] IS NULL OR			-- Задание не взято в работу или ...
					[t].[UserID] = [ru].[UserID] OR	-- ... задание взято в работу пользователем как исполнителем или ...
					[rd].[DeputyID] IS NOT NULL\)		-- ... задание взято в работу тем\, кого пользователь замещает в этой роли
					-- в случае отсутствия данные в родительской вьюхе\, это условие не проверяется
					\#if\(Department && Department.ValueCount > 0\) \{
						\#if\(Department.Value == Guid.Parse\("13769A42-E766-4407-8F9B-758820972EE5"\)\) \{
					AND [rud].[ID] IS NULL
						\} \{
					\#param\(Department\, [rud].[ID]\)
						\}
					\} \{
					AND [rud].[ID] = @NoRowsInParentDepartment
					\}
					\#param\(SelUser\, [ru].[UserID]\)
				\)
			SELECT [t].*
			FROM \(
				SELECT
					[t].*\,
					row_number\(\) OVER \(ORDER BY CASE WHEN [t].[UserName] IS NULL THEN 1 ELSE 0 END\, \#order_by\) AS [rn]
				FROM \(
					SELECT
						[t].[UserID]\,
						[t].[UserName]\,
						COALESCE\(max\(CASE WHEN [t].[Column] = 'id_00_cnt' THEN [t].[Value] END\)\, 0\)	AS [New]\,
						COALESCE\(max\(CASE WHEN [t].[Column] = 'id_01_cnt' THEN [t].[Value] END\)\, 0\)	AS [NewDelayed]\,
						COALESCE\(max\(CASE WHEN [t].[Column] = 'id_01_avg' THEN [t].[Value] END\)\, 0\)	AS [NewAvgDelayPeriod]\,
						COALESCE\(max\(CASE WHEN [t].[Column] = 'id_10_cnt' THEN [t].[Value] END\)\, 0\)	AS [InWork]\,
						COALESCE\(max\(CASE WHEN [t].[Column] = 'id_11_cnt' THEN [t].[Value] END\)\, 0\)	AS [InWorkDelayed]\,
						COALESCE\(max\(CASE WHEN [t].[Column] = 'id_11_avg' THEN [t].[Value] END\)\, 0\)	AS [InWorkAvgDelayPeriod]\,
						COALESCE\(max\(CASE WHEN [t].[Column] = 'id_20_cnt' THEN [t].[Value] END\)\, 0\)	AS [Postponed]\,
						COALESCE\(max\(CASE WHEN [t].[Column] = 'id_21_cnt' THEN [t].[Value] END\)\, 0\)	AS [PostponedDelayed]\,
						COALESCE\(max\(CASE WHEN [t].[Column] = 'id_21_avg' THEN [t].[Value] END\)\, 0\)	AS [PostponedAvgDelayPeriod]\,
						COALESCE\(max\(CASE WHEN [t].[Column] = 'id__cnt' THEN [t].[Value] END\)\, 0\)	AS [Total]
					FROM \(
						SELECT
							COALESCE\([b].[UserID]\, @TotalID\) AS [UserID]\,
							[b].[UserName]\,
							[c].[Column]\,
							[c].[Value]
						FROM \(
							SELECT
								[td].[UserID]\,
								[td].[UserName]\,
								[td].[StateID]\,
								[td].[DelayIndex]\,
								sum\([td].[sum]\) / sum\([td].[cnt]\)	AS [AvgHDelay]\,
								sum\([td].[cnt]\)					AS [cnt]
							FROM [TasksAndDepts]				AS [td]
							GROUP BY GROUPING SETS \(
								\(\)\,
								\([UserID]\, [UserName]\)\,
								\([UserID]\, [UserName]\, [StateID]\, [DelayIndex]\)\,
								\([StateID]\, [DelayIndex]\)
								\)
							\) AS [b]
						CROSS APPLY \(
							SELECT 'id_' + COALESCE\(CAST\([b].[StateID] AS varchar\)\, ''\) + COALESCE\(CAST\([b].[DelayIndex] AS varchar\)\, ''\) + '_cnt'\, [cnt]
							UNION ALL
							SELECT 'id_' + COALESCE\(CAST\([b].[StateID] AS varchar\)\, ''\) + COALESCE\(CAST\([b].[DelayIndex] AS varchar\)\, ''\) + '_avg'\, [AvgHDelay]
							\) AS [c] \([Column]\, [Value]\)
						\) AS [t]
					GROUP BY [t].[UserID]\, [t].[UserName]
					\) AS [t]
				\) AS [t]
			\#if\(PageOffset\) \{
			WHERE [t].[rn] >= \#param\(PageOffset\) AND [t].[rn] < \(\#param\(PageOffset\) + \#param\(PageLimit\)\)
			\}
			ORDER BY [t].[rn];
		}
		#pg_query {
			DECLARE
				"total_id"						uuid;
				"system_user_id"				uuid;
				"no_rows_in_parent_department"	uuid;

				\#if\(Normal\) \{
				"now_timestamp"					timestamptz;
				\}
			BEGIN
				"total_id"						= '00000000-0000-0000-0000-000000000000';
				"system_user_id"				= '11111111-1111-1111-1111-111111111111';
				"no_rows_in_parent_department"	= '11111111-1111-1111-1111-111111111111'; -- используется для предотвращение показа строк\, если их нет в родительском представлении

				\#if\(Normal\) \{
				"now_timestamp"					= current_timestamp;
				\}

				RETURN QUERY
				WITH "tasks_and_depts" \("UserID"\, "UserName"\, "StateID"\, "DelayIndex"\, "sum"\, "cnt"\) AS \(
					SELECT
						"pr"."ID"\,
						"pr"."Name"\,
						"t"."StateID"\,
						"t"."DelayIndex"\,
						"t"."sum"\,
						"t"."cnt"
					FROM \(
						SELECT
							"t"."StateID"\,
							"t"."RoleID"\,
							"t"."UserID"\,
							"t"."DelayIndex"\,
							sum\("t"."Quant"\)	AS "sum"\,
							count\(*\)			AS "cnt"
						FROM \(
							SELECT
								"t"."StateID"\,
								"t"."RoleID"\,
								"t"."UserID"\,
								CASE WHEN "qq1"."QuantNumber" - "qq"."QuantNumber" > 0
									THEN 1
									ELSE 0
								END AS "DelayIndex"\,
								\("qq1"."QuantNumber" - "qq"."QuantNumber"\) / 4 AS "Quant"
							FROM "Tasks" AS "t"
							\#if\(TypeParam\) \{
							LEFT JOIN "DocumentCommonInfo" AS "i"
								ON "i"."ID" = "t"."ID"
							\}
							LEFT JOIN LATERAL \(
								SELECT "q"."QuantNumber"
								FROM "CalendarQuants" AS "q"
								WHERE "q"."StartTime" <= "t"."Planned" + "t"."TimeZoneUtcOffsetMinutes" * interval '1 minute'
								ORDER BY "q"."StartTime" DESC
								LIMIT 1
								\) AS "qq" ON true
							LEFT JOIN LATERAL \(
								SELECT "q"."QuantNumber"
								FROM "CalendarQuants" AS "q"
								WHERE "q"."StartTime" <= "now_timestamp" + "t"."TimeZoneUtcOffsetMinutes" * interval '1 minute'
								ORDER BY "q"."StartTime" DESC
								LIMIT 1
								\) AS "qq1" ON true
							WHERE true
								\#param\(EndDate\, "t"."Planned"\)
								\#param\(CreationDate\, "t"."Created"\)
								\#param\(TaskType\, "t"."TypeID"\)
								\#if\(TypeParam\) \{
								AND COALESCE\("i"."DocTypeID"\, "i"."CardTypeID"\) = \#param\(TypeParam\)
								\}
							\) AS "t"
						GROUP BY "t"."StateID"\, "t"."RoleID"\, "t"."UserID"\, "t"."DelayIndex"
						\) AS "t"
					INNER JOIN "RoleUsers" AS "ru"
						-- Или сотрудник входит в роль и задание на в работе\, или он является исполнителем задания
						ON "ru"."ID" = COALESCE\("t"."UserID"\, "t"."RoleID"\)
						AND "ru"."UserID" <> "system_user_id"
					INNER JOIN "PersonalRoles" AS "pr"
						ON "pr"."ID" = "ru"."UserID" -- решает задвоение результатов при переименовании пользователей
					/*
					Джойн для отработки замещения конкретного пользователя в ролях для заданий в работе \(в т.ч. персональной роли\).
					Пользователь будет видеть задания в работе тех сотрудников\, кого он замещает.
					Система заботится о том чтобы периоды замещения не пересекались\, поэтому строчки не множатся.
					Используется OUTER APPLY вместо LEFT JOIN\, т.к. в RoleDeputies могут быть несколько строк по разным замещаемым сотрудникам.
					*/
					LEFT JOIN LATERAL \(
						SELECT "rd"."DeputyID"
						FROM "RoleDeputies" AS "rd"
						WHERE "rd"."ID" = "t"."RoleID"										-- Заместитель видит только задания в работе на определенную роль
							AND "rd"."IsActive" = true										-- Только активные замещения - выставляется сервисом
							AND "rd"."DeputyID" = "ru"."UserID"								-- для работы отчета по заданиям других пользователей
							AND \(
								"rd"."TypeID" = 0 AND "t"."UserID" IS NULL OR				-- Замещение в стат. роли\, не в работе
								"rd"."TypeID" = 0 AND "t"."UserID" = "rd"."DeputizedID" OR	-- Замещение в стат. роли\, в работе у замещаемого
								"rd"."TypeID" = 1\)											-- Замещение персональной роли
						LIMIT 1
						\) AS "rd" ON true
					LEFT JOIN "RoleUsers" AS "rud"
						ON "rud"."TypeID" = 2
						AND "rud"."UserID" = "ru"."UserID"
					LEFT JOIN "Roles" AS "r"
						ON "r"."ID" = "rud"."ID"
					INNER JOIN \(
						-- Для показа только заданий в своих подразделениях\, если юзер - глава\, или только своих.
						-- Работает только при наличии метароли - все сотрудники на все уровни вглубь
						SELECT "ru"."UserID"
						FROM "DepartmentRoles" AS "dr"
						INNER JOIN "MetaRoles" AS "mr"
							ON "mr"."IDGuid" = "dr"."ID"
						INNER JOIN "RoleUsers" AS "ru"
							ON "ru"."ID" = "mr"."ID"
						WHERE "dr"."HeadUserID" = \#param\(CurrentUserID\)

						UNION

						-- Добавляем сотрудников подразделений\, для которых нет метаролей и текущий = руководитель
						SELECT "ru"."UserID"
						FROM "DepartmentRoles" AS "dr"
						INNER JOIN "RoleUsers" AS "ru"
							ON "ru"."ID" = "dr"."ID"
						WHERE "dr"."HeadUserID" = \#param\(CurrentUserID\)
							AND NOT EXISTS \(
								SELECT NULL
								FROM "MetaRoles" AS "mr"
								WHERE "mr"."IDGuid" = "dr"."ID"\)

						UNION

						-- Может смотреть отчёт по своим заданиям
						SELECT \#param\(CurrentUserID\)

						UNION

						-- Учёт карточек настройки прав на отчёт
						\#if\(Administrator\) \{
						-- Админ видит всех сотрудников\, входящих в роль "Все сотрудники"\, т.е. обычно всех\, кроме System
						SELECT "rua"."UserID"
						FROM "RoleUsers" AS "rua"
						WHERE "rua"."ID" = '7ff52dc0-ff6a-4c9d-ba25-b562c370004d'
						\} \{
						SELECT DISTINCT "rup"."UserID"
						FROM "ReportRolesActive" AS "rra"
						INNER JOIN "RoleUsers" AS "rua"
							ON "rua"."ID" = "rra"."RoleID"
						INNER JOIN "ReportRolesPassive" AS "rrp"
							ON "rrp"."ID" = "rra"."ID"
						INNER JOIN "RoleUsers" AS "rup"
							ON "rup"."ID" = "rrp"."RoleID"
						WHERE "rua"."UserID" = \#param\(CurrentUserID\)
						\}
						\) AS "wr" ON "wr"."UserID" = "ru"."UserID"
					\#if\(Role\) \{
					INNER JOIN "RoleUsers" AS "fr"
						ON "fr"."UserID" = "ru"."UserID"
						\#param\(Role\, "fr"."ID"\)
					\}
					WHERE \(
						"t"."UserID" IS NULL OR			-- Задание не взято в работу или ...
						"t"."UserID" = "ru"."UserID" OR	-- ... задание взято в работу пользователем как исполнителем или ...
						"rd"."DeputyID" IS NOT NULL\)	-- ... задание взято в работу тем\, кого пользователь замещает в этой роли
						-- в случае отсутствия данные в родительской вьюхе\, это условие не проверяется
						\#if\(Department && Department.ValueCount > 0\) \{
							\#if\(Department.Value == Guid.Parse\("13769A42-E766-4407-8F9B-758820972EE5"\)\) \{
						AND "rud"."ID" IS NULL
							\} \{
						\#param\(Department\, "rud"."ID"\)
							\}
						\} \{
						AND "rud"."ID" = "no_rows_in_parent_department"
						\}
						\#param\(SelUser\, "ru"."UserID"\)
					\)
				SELECT "t".*
				FROM \(
					SELECT
						"t".*\,
						row_number\(\) OVER \(ORDER BY CASE WHEN "t"."UserName" IS NULL THEN 1 ELSE 0 END\, \#order_by\) AS "rn"
					FROM \(
						SELECT
							"t"."UserID"\,
							"t"."UserName"\,
							COALESCE\(max\(CASE WHEN "t"."Column" = 'id_00_cnt' THEN "t"."Value" END\)\, 0\)	AS "New"\,
							COALESCE\(max\(CASE WHEN "t"."Column" = 'id_01_cnt' THEN "t"."Value" END\)\, 0\)	AS "NewDelayed"\,
							COALESCE\(max\(CASE WHEN "t"."Column" = 'id_01_avg' THEN "t"."Value" END\)\, 0\)	AS "NewAvgDelayPeriod"\,
							COALESCE\(max\(CASE WHEN "t"."Column" = 'id_10_cnt' THEN "t"."Value" END\)\, 0\)	AS "InWork"\,
							COALESCE\(max\(CASE WHEN "t"."Column" = 'id_11_cnt' THEN "t"."Value" END\)\, 0\)	AS "InWorkDelayed"\,
							COALESCE\(max\(CASE WHEN "t"."Column" = 'id_11_avg' THEN "t"."Value" END\)\, 0\)	AS "InWorkAvgDelayPeriod"\,
							COALESCE\(max\(CASE WHEN "t"."Column" = 'id_20_cnt' THEN "t"."Value" END\)\, 0\)	AS "Postponed"\,
							COALESCE\(max\(CASE WHEN "t"."Column" = 'id_21_cnt' THEN "t"."Value" END\)\, 0\)	AS "PostponedDelayed"\,
							COALESCE\(max\(CASE WHEN "t"."Column" = 'id_21_avg' THEN "t"."Value" END\)\, 0\)	AS "PostponedAvgDelayPeriod"\,
							COALESCE\(max\(CASE WHEN "t"."Column" = 'id__cnt' THEN "t"."Value" END\)\, 0\)		AS "Total"
						FROM \(
							SELECT
								COALESCE\("b"."UserID"\, "total_id"\) AS "UserID"\,
								"b"."UserName"\,
								"c"."Column"\,
								"c"."Value"
							FROM \(
								SELECT
									"td"."UserID"\,
									"td"."UserName"\,
									"td"."StateID"\,
									"td"."DelayIndex"\,
									sum\("td"."sum"\) / sum\("td"."cnt"\)		AS "AvgHDelay"\,
									sum\("td"."cnt"\)						AS "cnt"
								FROM "tasks_and_depts"					AS "td"
								GROUP BY GROUPING SETS \(
									\(\)\,
									\("td"."UserID"\, "td"."UserName"\)\,
									\("td"."UserID"\, "td"."UserName"\, "td"."StateID"\, "td"."DelayIndex"\)\,
									\("td"."StateID"\, "td"."DelayIndex"\)
									\)
								\) AS "b"
							CROSS JOIN LATERAL \(
								SELECT 'id_' || COALESCE\(CAST\("b"."StateID" AS varchar\)\, ''\) || COALESCE\(CAST\("b"."DelayIndex" AS varchar\)\, ''\) || '_cnt'\, "cnt"\:\:bigint
								UNION ALL
								SELECT 'id_' || COALESCE\(CAST\("b"."StateID" AS varchar\)\, ''\) || COALESCE\(CAST\("b"."DelayIndex" AS varchar\)\, ''\) || '_avg'\, "AvgHDelay"\:\:bigint
								\) AS "c" \("Column"\, "Value"\)
							\) AS "t"
						GROUP BY "t"."UserID"\, "t"."UserName"
						\) AS "t"
					\) AS "t"
				\#if\(PageOffset\) \{
				WHERE "t"."rn" >= \#param\(PageOffset\) AND "t"."rn" < \(\#param\(PageOffset\) + \#param\(PageLimit\)\)
				\}
				ORDER BY "t"."rn";
			END;
		}
		#role(RoleID:7ff52dc0-ff6a-4c9d-ba25-b562c370004d, ViewID:871ad32d-0846-448c-beb2-85c1d357177b) 
	}
}