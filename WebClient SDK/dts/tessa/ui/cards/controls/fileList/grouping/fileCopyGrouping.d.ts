import { FileGrouping, GroupInfo } from './fileGrouping';
import { FileViewModel } from '../fileViewModel';
export declare class FileCopyGrouping extends FileGrouping {
    constructor(name: string, caption: string, getFiles: () => ReadonlyArray<FileViewModel>, isCollapsed?: boolean);
    private _getFiles;
    readonly noCopyGroupName = "NoCopyGroup";
    readonly noCopyGroupCaption = "$UI_Controls_FilesControl_NoCopy";
    getGroupInfo(viewModel: FileViewModel): GroupInfo;
    clone(): FileCopyGrouping;
}
